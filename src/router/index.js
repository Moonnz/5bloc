import Vue from 'vue';
import VueRouter from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import ListTokens from '@/views/ListTokens';
import CreateToken from '@/views/CreateToken';
import Withdraw from '@/views/Withdraw';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: HelloWorld,
        meta: {
            title: 'Crypt\'IMMO',
        },
    },
    {
        path: '/tokens',
        name: 'tokens',
        component: ListTokens,
        meta: {
            title: 'Crypt\'IMMO tokens',
        },
    },
    {
        path: '/create',
        name: 'createToken',
        component: CreateToken,
        meta: {
            title: 'Crypt\'IMMO create token',
        },
    },
    {
        path: '/withdraw',
        name: 'withdraw',
        component: Withdraw,
        meta: {
            title: 'Crypt\'IMMO withdraw',
        },
    }
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
    if(!nearestWithMeta) return next();
        nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
        });

        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    .forEach(tag => document.head.appendChild(tag));

    next();
});

export default router;
